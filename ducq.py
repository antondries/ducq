"""

:abbr:`Ducq (a ducq is an animal that does quacq)` is an implementation of the QuAcq algorithm.
QuAcq is an algorithm for learning constraint networks by asking partial queries to a user.


Algorithm
---------

The QuAcq algorithm is built around four core functions.

``find_example(learned, candidates)``
  Finds an example that satisfies all learned constraints and violates at least one of the \
  candidate constraints.  Ducq uses a underlying constraint solver to generate such examples.

``find_scope(example, variables_r, variable_y, ask_query)``
  Given a negative example, finds a subset of variables (i.e. a scope) that causes the example to \
  be classified as negative by the user.

``find_constraint(example, scope, constraints)``
  Given a negative example and a scope, find a candidate constraint using the variables in scope \
  and that is violated on the example.

``ask_query(example)``
  Ask the user to classify (a partial) example as definitely negative or maybe positive.

Specifying a learning task
--------------------------

Specifying a learning task consists of specifying the following items:

 * An oracle (which can answer questions about partial examples).
 * Variables and domains.
 * (optional) The constraint language.

See ``examples/`` for some examples.

Settings
--------

Finding an example
++++++++++++++++++

One of the key functions is finding an example that satisfies some constraints and violates others.
In order to find such an example, Ducq uses an underlying constraint solver.
The solver can be selected with the command line option ``--solver``.

Currently, two solvers are supported:

 * Gecode_
 * MinisatID_


.. _Gecode: http://www.gecode.org/
.. _MinisatID: https://dtai.cs.kuleuven.be/software/minisatid

Finding examples that satisfy all constraints but violate a few other can become computationally \
expensive when the first set allows many solutions, but there are only few that violate the \
additional constraints. Ducq can use two strategies to find a suitable example:

postprocess
  Let the constraint solver enumerate all solutions that satisfy the given constraints and check \
  each solution individually as a post-processing step.

implied
  For each remaining candidate constraint, add its inverse to the set of constraints, and try to \
  find a solution. If not solution is found, then the constraint is implied by the others and can \
  be removed. Otherwise, the solution that is found can be used as the next example.

The strategy can be selected using the command line option ``--strategy``.


Early termination
+++++++++++++++++

For some problems, the process can become very slow when most constraints have been learned.
This can be prevented by setting early stopping conditions that abort the search for an example \
after some time. Note that by using these conditions, the set of learned constraints might not \
be complete.

The available stopping conditions depend on the selected search strategy:

postprocess
  By setting the ``--max-examples`` argument, one can set the maximal number of solver solutions \
  that Ducq should examine before assuming that all candidate constraints are implied.
  Note that, the counter is reset for each call to ``find_example``, but once this condition is \
  met, the algorithm finishes immediately.

implied
  By setting the ``--timeout`` argument, one can set the maximal amount of time the constraint \
  solver can use to find a solution that violates a given constraint.
  Note that when this condition is met, only the one constraint under consideration is discarded \
  and the process continues.


API documentation
-----------------

.. py:currentmodule:: ducq

Defining Oracles
++++++++++++++++

An *oracle* is an object that can answer queries about (partial) examples.
This could be a human user or a black-box constraint model that can answer membership queries.

.. autoclass:: ducq.Oracle
    :members:

.. autoclass:: ducq.ModelBasedOracle
    :members:

.. autoclass:: ducq.FunctionBasedOracle
    :members:

.. autoclass:: ducq.SolutionBasedOracle
    :members:

.. autoclass:: ducq.CLIOracle
    :members:

Describing learning tasks
+++++++++++++++++++++++++

A learning task consists of three main components:

 * the set of variables in the target problem and their domains
 * the oracle that can answer question on whether an example is a (partial) solution or not
 * the set of constraints that should be explored

The learning task can be specified by creating a :class:`LearningTask` object.

.. autoclass:: ducq.LearningTask
    :members:

Constraints and Examples
++++++++++++++++++++++++

.. autoclass:: ducq.ConstraintSet
    :members:

.. autoclass:: ducq.Constraint
    :members:

.. autoclass:: ducq.CompareConstraint
    :members:

.. autoclass:: ducq.CompoundConstraint
    :members:

.. autoclass:: ducq.FuncDesc
    :members:

.. autoclass:: ducq.Example
    :members:


Additional methods
++++++++++++++++++

.. data:: ducq.DEFAULT_SOLVER

.. data:: ducq.SOLVERS

.. autofunction:: ducq.add_arguments

.. autofunction:: ducq.call_fzn
"""

from __future__ import print_function


__author__ = 'Anton Dries'


import subprocess
import time
import os
import sys
import tempfile
import resource
from collections import OrderedDict, namedtuple

import inspect


DEFAULT_SOLVER = 'gecode'

#: Available solvers.
SOLVERS = {
    'minisatid': ['minisatid', '-f=fz', '--verbosity=0'],
    'gecode': ['fzn-gecode', '-p', '0']
}


class Oracle(object):
    """Base class for all oracles that answer queries about partial examples."""

    def ask(self, example):
        """Ask question.

        :param example: (partial) example that the oracle should classify. \
        Non-assigned variables should be set to None.
        :type example: :class:`Example`
        :return: False if the assignments of the example violate a constraint, True otherwise.
        :rtype: bool
        """
        raise NotImplementedError('Oracle.ask is an abstract method.')


class ModelBasedOracle(Oracle):
    """Oracle based on a known constraint model.

    :param constraints: set of constraints that describe the target problem
    :type constraints: :class:`ConstraintSet`
    """

    def __init__(self, constraints):
        Oracle.__init__(self)
        self.constraints = constraints

    def ask(self, example):
        """Ask question.

        :param example: (partial) example that the oracle should classify. \
        Non-assigned variables should be set to None.
        :type example: :class:`Example`
        :return: False if the assignments of the example violate a constraint, True otherwise.
        :rtype: bool
        """
        return not self.constraints.is_violated_by(example)


class FunctionBasedOracle(Oracle):
    """Oracle based on a Python function.

    :param function: function that answers the query
    :type function: (:class:`Example`) -> bool
    """

    def __init__(self, function):
        Oracle.__init__(self)
        self.function = function

    def ask(self, example):
        """Ask question.

        :param example: (partial) example that the oracle should classify. \
        Non-assigned variables should be set to None.
        :type example: :class:`Example`
        :return: False if the assignments of the example violate a constraint, True otherwise.
        :rtype: bool
        """
        return self.function(example)


class CLIOracle(Oracle):
    """Oracle based on interaction with the user through the command line.

    :param formatter: function that pretty prints the partial solution (default: str)
    :type formatter: (:class:`Example`) -> str
    """

    def __init__(self, formatter=str):
        Oracle.__init__(self)
        self.formatter = formatter
        self._response_cache = {}

    def _input(self):
        """Read input from the command line in a Python2 and 3 compatible way.

        :return: string read from the input
        """
        if sys.version_info.major == 2:
            # noinspection PyCompatibility
            return raw_input('> ')
        else:
            return input('> ')

    def ask(self, example):
        """Ask question.

        :param example: (partial) example that the oracle should classify. \
        Non-assigned variables should be set to None.
        :type example: :class:`Example`
        :return: False if the assignments of the example violate a constraint, True otherwise.
        :rtype: bool
        """
        if example in self._response_cache:
            print ('Skipped a question.')
            return self._response_cache[example]
        else:
            print ('Is the following assignment a (partial) solution to your problem?')
            print (self.formatter(example))
            try:
                answer = self._input()
                if answer[0] in 'yY':
                    self._response_cache[example] = True
                    return True
                else:
                    self._response_cache[example] = False
                    return False
            except IOError:
                return False
            except IndexError:
                return False


class SolutionBasedOracle(Oracle):
    """Oracle based on the known (unique) solution to the problem.

    :param solution: unique solution to the problem
    :type solution: Sequence[int]
    """

    def __init__(self, solution):
        Oracle.__init__(self)
        self.solution = solution

    def ask(self, example):
        """Ask question.

        :param example: (partial) example that the oracle should classify. \
        Non-assigned variables should be set to None.
        :type example: :class:`Example`
        :return: False if the assignments of the example violate a constraint, True otherwise.
        :rtype: bool
        """
        for e, s in zip(example, self.solution):
            if e is not None and s is not None:
                if e != s:
                    return False
        return True


class LearningTask(object):
    """Describes a learning problem
    This includes variables and domains, bias and the oracle.

    :param domains: domains of the variables in the problem; variables are derived from this
    :type domains: list[list[int]]
    :param oracle: oracle that can answer queries about partial examples
    :type oracle: :class:`Oracle`
    :param language: set of constraint types that should be explored
    :type language: list[:class:`ConstraintType`]
    :param options: additional options
    """

    def __init__(self, domains, oracle, bias, verbose=0, **options):
        self.__variables = set(range(0, len(domains)))
        self.__domains = {v: d for v, d in enumerate(domains)}
        self.__oracle = oracle
        self.__bias = ConstraintSet(bias)
        self.options = options
        self.verbose = verbose
        self.statistics = None
        self.reset()

    def debug(self, level, *message):
        """Print a debug message if the level is lower than the verbosity level.

        :param level: level of the message
        :param message: parts of the message
        """
        if level < self.verbose:
            print (*message, file=sys.stderr)

    def reset(self):
        """Reset the learning problem.
        This reinitializes the bias and statistics.
        """
        self.statistics = OrderedDict()
        self.statistics['example_main'] = 0
        self.statistics['example_findC'] = 0
        self.statistics['example_generate1'] = 0
        self.statistics['example_generate2'] = 0
        self.statistics['query_main'] = 0
        self.statistics['query_main_yes'] = 0
        self.statistics['query_main_no'] = 0
        self.statistics['query_findS_skip'] = 0
        self.statistics['query_findS'] = 0
        self.statistics['query_findS_yes'] = 0
        self.statistics['query_findS_no'] = 0
        self.statistics['query_findC'] = 0
        self.statistics['query_findC_yes'] = 0
        self.statistics['query_findC_no'] = 0
        self.statistics['constraint_found'] = 0

    @property
    def variables(self):
        """Variables in the model.

        :rtype: set of int
        """
        return self.__variables

    @property
    def domains(self):
        """Domains of variables in the model.

        :rtype: dict[int, list[int]]
        """
        return self.__domains

    @property
    def oracle(self):
        """Oracle for this problem.

        :rtype: :class:`Oracle`
        """
        return self.__oracle

    @property
    def bias(self):
        """Set of candidate constraints.

        :rtype: :class:`ConstraintSet`
        """
        return self.__bias

    def find_scope(self, example, variables_r, variables_y, ask_query):
        """For a given negative example, find a subset of variables that cause the example to be \
        negative.

        :param example:
        :type example: :class:`Example`
        :param variables_r:
        :type variables_r: set[int]
        :param variables_y:
        :type variables_y: set[int]
        :param ask_query:
        :type ask_query: bool
        :return: scope
        :rtype: set[int]
        """

        if ask_query:
            self.statistics['query_findS'] += 1
            ex = example.project(variables_r)
            self.debug(3, 'Find scope example:', ex)
            if self.oracle.ask(ex):
                self.debug(3, ' is positive')
                self.statistics['query_findS_yes'] += 1
                self.bias.remove_violated(ex)
            else:
                self.debug(3, ' is negative')
                self.statistics['query_findS_no'] += 1
                return set()
        else:
            self.statistics['query_findS_skip'] += 1
        if len(variables_y) == 1:
            return variables_y
        variables_y1, variables_y2 = split_set(variables_y)
        variables_s1 = self.find_scope(example,
                                       variables_r | variables_y1, variables_y2, True)
        variables_s2 = self.find_scope(example,
                                       variables_r | variables_s1, variables_y1, bool(variables_s1))
        return variables_s1 | variables_s2

    def find_constraint(self, example, scope, constraints):
        """Given a negative example and a scope, find a candidate constraint using the \
        variables from scope that eliminates the example.

        :param example: example classified as negative by the oracle
        :type example: :class:`Example`
        :param scope: set of variables of interest
        :type scope: set[int]
        :param constraints: constraints found so far
        :type constraints: :class:`ConstraintSet`
        :return: constraint that is violated by the example
        :rtype: :class:`Constraint`
        """

        delta = self.bias.compute_violated(example.project(scope), scope)

        if not delta:
            return 0
        else:
            while True:
                example1 = self.select_example2(constraints, delta, scope)
                self.statistics['example_findC'] += 1
                self.debug(3, 'Find constraint example', example1)
                if example1 is None:
                    if not delta:
                        return None
                    else:
                        return delta.any()
                self.statistics['query_findC'] += 1
                if self.oracle.ask(example1):
                    self.debug(3, ' is positive')
                    self.statistics['query_findC_yes'] += 1
                    self.bias.remove_violated(example1)
                    delta.remove_violated(example1)
                else:
                    self.debug(3, ' is negative')
                    self.statistics['query_findC_no'] += 1
                    violated = self.bias.compute_violated(example1, scope)
                    if violated is None:
                        scope = self.find_scope(example1, set(), scope, False)
                        self.debug(3, ' smaller scope found', scope)
                        return self.find_constraint(example1, scope, constraints)
                    else:
                        delta = delta & violated
                        # delta = delta.conjoin(violated)

    def select_example1(self, constraints):
        """
        Select an example that satisfies all given constraints but violates at least one candidate \
        constraint.

        :param constraints: set of constraints learned so far
        :type constraints: :class:`ConstraintSet`
        :return: an example
        :rtype: :class:`Example`
        """
        if len(self.bias) == 0:
            return None
        strategy = self.options.get('strategy', 'postprocess')
        if strategy == 'postprocess':
            return self._select_example1_with_postprocess(constraints)
        elif strategy == 'implied':
            return self._select_example1_with_implied_check(constraints)
        elif strategy == 'maxfail':
            return self._select_example1_with_max_negatives(constraints)
        else:
            return ValueError("Unknown strategy '%s'." % strategy)

    def _select_example1_with_max_negatives(self, constraints):
        gen = constraints.generate_examples(self, findall=False, negative=self.bias)
        for example in gen:
            gen.close()  # Explicit generator close to bypass PyPy restriction
            return example
        return None

    def _select_example1_with_postprocess(self, constraints):
        gen = constraints.generate_examples(self, findall=True)
        for example in gen:
            if self.bias.is_violated_by(example):
                gen.close()
                return example
            self.statistics['example_generate1'] += 1
        return None

    def _select_example1_with_implied_check(self, constraints):
        for n_constraint in self.bias:
            discard = 'remove'
            try:
                gen = constraints.generate_examples(self, findall=False,
                                                    negative=ConstraintSet([n_constraint]))
                for example in gen:
                    # print ('found', example)
                    gen.close()
                    return example
            except ProcessInterrupted:
                discard = 'discard'
            self.statistics['example_generate1'] += 1
            # No example can be found that violates n_constraint =>> it is implied.
            self.debug(1, discard, n_constraint, len(self.bias))
            self.bias.remove_implied(ConstraintSet([n_constraint]))
        return None

    def _select_example1_with_implied_check_batch(self, constraints, batchsize=10):
        batch = []
        i = 0
        for n_constraint in self.bias:
            i += 1
            batch.append(n_constraint)

            if len(batch) == batchsize or i == len(self.bias):
                gen = constraints.generate_examples(self, findall=False,
                                                    negative=ConstraintSet(batch))
                for example in gen:
                    gen.close()
                    return example
                self.statistics['example_generate1'] += 1
                # No example can be found that violates n_constraint =>> it is implied.
                self.debug(1, 'remove', batch)
                self.bias.remove_implied(ConstraintSet(batch))
                batch = []
        return None

    def select_example2(self, constraints1, constraints2, scope):
        """Find an example that satisfies all constraints of the first set and that splits the \
        second set.

        :param constraints1: set of learned constraints
        :type constraints1: :class:`ConstraintSet`
        :param constraints2: set of candidate constraints
        :type constraints2: :class:`ConstraintSet`
        :param scope: only consider constraints with these variables
        :type scope: set[int]
        :return: an example
        :rtype: :class:`Example`
        """
        gen = constraints1.generate_examples(self, scope=scope, findall=True)
        for example in gen:
            if constraints2.partitioned_by(example, scope):
                gen.close()
                return example
            self.statistics['example_generate2'] += 1
        return None

    def has_solutions(self, constraints):
        """Check whether the set of constraints has any solutions left.

        :param constraints:
        :type constraints: :class:`ConstraintSet`
        :return:
        :rtype: bool
        """
        gen = constraints.generate_examples(self, findall=False)
        for _ in gen:
            gen.close()
            return True
        return False

    def quacq(self):
        """Execute the QuAcq learning algorithm.

        :return: set of learned constraints
        :rtype: :class:`ConstraintSet`
        """
        output = ConstraintSet()
        iteration = 0

        self.debug(1, 'Start learning: (size of bias: %s)' % len(self.bias))
        self.debug(3, self.bias)

        while True:
            iteration += 1
            if not self.has_solutions(output):
                self.debug(0, 'Learned model has no solutions.')
                return None  # the search space has collapsed
            example = self.select_example1(output)
            self.statistics['example_main'] += 1
            self.debug(1, 'Example:', example)
            if example is None:
                return output  # we have reached convergence
            self.statistics['query_main'] += 1
            if self.oracle.ask(example):  # ask the oracle
                self.debug(2, ' is positive')
                self.statistics['query_main_yes'] += 1
                self.bias.remove_violated(example)
            else:
                self.debug(2, ' is negative')
                self.statistics['query_main_no'] += 1
                scope = self.find_scope(example, set(), self.variables, False)
                self.debug(2, ' scope:', scope)
                constraint = self.find_constraint(example, scope, output)
                if constraint is None:
                    self.debug(0, 'No constraint found.')
                    return None
                elif constraint == 0:
                    # We are done?
                    return output
                else:
                    self.statistics['constraint_found'] += 1
                    for c in constraint:
                        self.bias.constraints[self.bias.constraints.index(c)] = None
                        output.add(c)

                    self.debug(0, 'Found constraint %s: %s' % (iteration, constraint))
                    self.bias.remove_implied(output)
                    self.debug(1, 'Remaining bias:', len(self.bias))

    def show_statistics(self):
        """Return a textual representation of statistics collected during a run of the algorithm.

        :rtype: str
        """
        return '\n'.join(["%18s : %5s" % it for it in self.statistics.items()])

    def run(self):
        """Execute the algorithm and output the results to stdout.

        """
        starttime = time.time()
        # try:
        constraints = self.quacq()

        if constraints is None:
            print ('Search collapsed')
        else:
            print ('Found %s constraints:' % len(constraints))
            print (constraints)
            outname = self.options.get('output')
            if outname is not None:
                print ('Final model: %s' % outname)
                fzn_v, fzn_c, fzn_s = constraints.to_fzn(self.variables, self.domains)
                with open(outname, 'w') as f:
                    print ('\n'.join(fzn_v + fzn_c + fzn_s), file=f)

        # except KeyboardInterrupt:
        #     print ('Interrupted')
        print ('Statistics:')
        print (self.show_statistics())
        print ('Total runtime: %.4fs' % (time.time()-starttime))


class ConstraintSet(object):
    """Represents a set of constraint.

    :param initial: initial constraints
    :type initial: list[:class:`Constraint`]
    """
    def __init__(self, initial=None):
        if initial is None:
            initial = []
        self.constraints = list(initial)
        """:type constraints: list of Constraint"""
        self.count = 0

    def remove_violated(self, example):
        """Remove constraints that are violated by the given example.

        :param example: known positive example
        :type example: Example
        """
        for i, c in enumerate(self.constraints):
            if c is not None and not c.check(example, True):
                self.constraints[i] = None

    def remove_implied(self, constraints):
        """Remove constraints that are implied by the given set of constraints.

        :param constraints: currently learned set of constraints
        :type constraints: ConstraintSet
        """
        for i, c in enumerate(self.constraints):
            if c is not None and c in constraints:
                self.constraints[i] = None

    def compute_violated(self, example, scope):
        """Compute the set of constraints that are violated by the given example for the \
        given scope.

        :param example: example
        :type example: :class:`Example`
        :param scope: variables to take into account
        :type scope: set[int]
        :return:
        """
        result = []
        for i, c in enumerate(self.constraints):
            if c is not None and not c.check(example.project(scope), True):
                if c.variables < scope:
                    return None
                result.append(c)
        return ConstraintSet(result)

    def intersect_violated(self, example):
        """Remove all constraints that are *not* violated by the given example.

        :param example: example
        :type example: :class:`Example`
        """
        for i, c in enumerate(self.constraints):
            if c is not None:
                if c.check(example, True):
                    # Not violated
                    self.constraints[i] = None
        return True

    def add(self, constraint):
        """Add a constraint to the set.

        :param constraint: constraint to add
        :type constraint: :class:`Constraint`
        """
        self.constraints.append(constraint)

    def any(self):
        """Return an arbitrary constraint from the set.

        :return: the first constraint in the set
        :rtype:  :class:`Constraint`
        """
        for c in self:
            return c
        return None

    def __str__(self):
        return '\n'.join(map(str, self))

    def partitioned_by(self, example, scope):
        """Check whether the set gets partitioned by the given example.
        A set is partitioned if the example satisfies at least one constraint and \
        violates at least one constraint.

        :param example: example
        :type example: :class:`Example`
        :param scope: only take into account variables that are defined for the given scope
        :type scope: set[int]
        :return: False if the example satisfies all constraints or violates all constraint; \
        True otherwise.
        :rtype: bool
        """
        has_true = False
        has_false = False
        for c in self.constraints:
            if c is not None and c.variables <= scope:
                if c.check(example, True):
                    has_true = True
                else:
                    has_false = True
                if has_true and has_false:
                    return True
        return False

    def is_violated_by(self, example):
        """Check whether the example violates any constraints.

        :param example: example
        :type example: :class:`Example`
        :return: True if at least one constraint is violated by the example; False otherwise.
        """
        for c in self.constraints:
            if c is not None and not c.check(example, True):
                return True
        return False

    def generate_examples(self, problem, scope=None, findall=False, negative=None):
        """Generate examples that satisfy all constraints on the given scope.

        :param problem: the learning task containing variable domains and other options
        :type problem: :class:`LearningTask`
        :param scope: the set of variables of interest
        :type scope: set[int]
        :param findall: find all examples instead of just one
        :type findall: bool
        :param negative: enforce that the example violates at least on of these constraints
        :type negative: :class:`ConstraintSet` or None
        :return: examples
        :rtype: generator[:class:`Example`]
        """

        if scope is not None:
            variables = scope
        else:
            variables = problem.variables

        fzn_variables, fzn_constraints, fzn_solve = self.to_fzn(variables, problem.domains)
        if negative is not None:
            fzn_variables_neg, fzn_constraints_neg, fzn_solve = \
                negative.to_fzn(variables, problem.domains, negated=True)
            fzn_variables += fzn_variables_neg
            fzn_constraints += fzn_constraints_neg
        fzn = fzn_variables + fzn_constraints + fzn_solve
        return call_fzn('\n'.join(fzn), len(problem.domains), findall, **problem.options)

    def to_fzn(self, variables, domains, output=True, negated=False):
        """Generate representation in FlatZinc for this set of constraints.

        :param variables: variables in the output
        :type variables: set[int]
        :param domains: domains of the variables
        :type domains: dict[int, list[int]]
        :param output: the variables should be marked as output variables
        :type output: bool
        :param negated: all constraints should be negated; variable definition will be skipped
        :type negated: bool
        :return: lists representing variable definition and constraints respectively
        :rtype: (list[str], list[str], list[str])
        """
        # Collect statements by type
        # (FlatZinc expects all variables definition before constraints).
        if output:
            fzn_output = ' :: output_var'
        else:
            fzn_output = ''

        fzn_variables = []
        fzn_constraints = []
        fzn_solve = ['solve satisfy;']

        if negated:
            if len(self) == 1:
                c_fzn_vars, c_fzn_cons = self.any().inverse().to_fzn(0)
                fzn_variables += c_fzn_vars
                fzn_constraints += c_fzn_cons
            else:
                r = 0
                for cn, c in enumerate(self):
                    if c.variables <= variables:
                        r_name = 'R%s' % r
                        ri_name = 'Ri%s' % r
                        fzn_variables.append('var bool: %s;' % r_name)
                        fzn_variables.append('var 0..1: %s;' % ri_name)

                        c_fzn_vars, c_fzn_cons = c.inverse().to_fzn(cn, r_name)
                        fzn_variables += c_fzn_vars
                        fzn_constraints += c_fzn_cons

                        fzn_constraints.append('constraint bool2int(%s,%s);' % (r_name, ri_name))
                        r += 1
                if r > 0:
                    s_name = 'S'
                    weights = ','.join('1' for _ in range(0, r)) + ',-1'
                    varnames = ','.join('Ri%s' % i for i in range(0, r)) + ',' + s_name
                    fzn_variables.append('var 1..%s: %s;' % (r, s_name))
                    fzn_constraints.append(
                        'constraint int_lin_eq([%s], [%s], 0);' % (weights, varnames))
                    fzn_solve = ['solve maximize S;']
        else:
            # Declare all variables in the model.
            for var in variables:
                dom = ','.join(map(str, domains[var]))
                fzn_variables.append('var {%s}: X%s%s;' % (dom, var, fzn_output))
            # Add all constraints.
            for cn, con in enumerate(self):
                if con.variables <= variables:
                    c_fzn_vars, c_fzn_cons = con.to_fzn(cn)
                    fzn_variables += c_fzn_vars
                    fzn_constraints += c_fzn_cons

        return fzn_variables, fzn_constraints, fzn_solve

    def conjoin(self, other):
        """Combine two sets of constraints by taking all pairwise conjunctions.

        :param other: other set of constraints
        :return:
        :rtype: ConstraintSet
        """
        result = set()
        for c1 in self:
            for c2 in other:
                c = c1 & c2
                if c is not None:
                    result.add(c)
        return ConstraintSet(list(result))

    def __and__(self, other):
        # Make pairwise conjunctions
        return self.conjoin(other)

    def __iter__(self):
        for x in self.constraints:
            if x is not None:
                yield x

    def __len__(self):
        return len([x for x in self.constraints if x is not None])

    def __contains__(self, item):
        return item in self.constraints


class Example(object):
    """Represents an example.

    :param example: content of the example
    :type example: list[int|None]
    :param projection: restrict example to these variables
    :type projection: set[int]
    """
    def __init__(self, example, projection=None):
        self.example = example
        self.projection = projection

    def project(self, variables):
        """Project the example on the given variables.

        :param variables: variables of interest
        :type variables: set[int]
        """
        return Example(self.example, variables)

    def __getitem__(self, item):
        if item >= len(self.example):
            raise IndexError()
        elif self.projection and item not in self.projection:
            return None
        else:
            return self.example[item]

    def __len__(self):
        return len(self.example)

    def __repr__(self):
        return repr(self.example)

    def __str__(self):
        return str([self[i] for i in range(0, len(self.example))])

    def __hash__(self):
        return hash(tuple([x for x in self]))

    def __eq__(self, other):
        if isinstance(other, Example):
            return [x for x in self] == [y for y in other]
        else:
            return False


class Constraint(object):
    """Base type for all constraints."""

    def check(self, example, default=None):
        """Verify whether the constraint holds on the example.

        :param example: the example to test on
        :type example: :class:`Example`
        :param default: default answer when the constraint is not applicable because relevant \
        variables are not assigned
        :return: True if the constraint is applicable and holds, False if the constraint is \
        applicable and fails, `default` if the constraint is not applicable
        """
        raise NotImplementedError('Constraint.check() is an abstract method')

    @property
    def variables(self):
        """Returns all the variables involved in the constraint.

        :return: set of variables in the constraint
        :rtype: set[int]
        """
        raise NotImplementedError('Constraint.variables is an abstract method')

    def inverse(self):
        """Create the inverse constraint.

        :return: a constraint representing the opposite of this constraint
        :rtype: :class:`Constraint`
        """
        raise NotImplementedError('Constraint.inverse() is an abstract method')

    def to_fzn(self, cn, reif=None):
        """Transform to a string of FlatZinc code.

        :param cn: unique identifier for constraint in the model
        :type cn: any
        :param reif: name of reification variable (None if not used)
        :type reif: str
        :return: two snippets of FlatZinc: variable definitions and constraints
        :rtype: str, str
        """
        raise NotImplementedError('Constraint.to_fzn() is an abstract method')

    def is_trivial(self):
        """Check whether the constraint is trivially true or false.
        This holds when both arguments are the same.

        :return: True if both arguments are identical (uses string-based comparison)
        :rtype: bool
        """
        raise NotImplementedError('Constraint.is_trivial() is an abstract method')

    def is_canonical(self):
        """Check whether the constraint is in canonical form.
        A constraint is in canonical form iff its arguments are in canonical form and \
        it is either non-symmetric or its arguments are in lexicographical form.
        (Lexicographical order is defined as order on the string representations.)

        For example:
            (note that 'eq' and '+' are symmetric and 'lt' and '-' are not)

            eq(X0,X1)    => True
            eq(X1,X0)    => False (symmetric and arguments not in lexicographical order)
            lt(X1,X0)    => True (not symmetric)
            eq(X1+X0,X2) => False (first argument not in canonical form)
            eq(X1-X0,X2) => True (first argument is not symmetric)
            eq(X2,X1-X0) => False (symmetric and arguments not in lexicographical order)

        :return: True if the constraint is in canonical form; False otherwise.
        :rtype: bool
        """
        raise NotImplementedError('Constraint.is_canonical() is an abstract method')

    def __and__(self, other):
        return self.conjoin(other)

    def __invert__(self):
        return self.inverse()

    def __iter__(self):
        return iter([self])

    def conjoin(self, other):
        """Combine two constraints with a conjunction.

        :param other: other constraint
        :type other: Constraint
        :return: constraint representing the conjunction of self and other
        """
        raise NotImplementedError('Constraint.conjoin() is an abstract method')


class CompareConstraint(Constraint):
    """Represents a binary comparison constraint with support for transformation functions.

    :param compare: comparison operator (one of CMP_{EQ|NE|LT|LE})
    :type compare: CompareDesc
    :param args: list of variables that are the inputs to the transformation functions (can be \
    more than two)
    :type args: list[int] or tuple[int]
    :param f1: transformation function to apply on first argument (or None for no transformation)
    :type f1: FuncDesc
    :param f1: transformation function to apply on second argument (or None for no transformation)
    :type f2: FuncDesc

    Example 1:
      We can define a transformation function that takes the difference of two variables and \
      compares it to a third value. The constraint has arity 3, and it's definition could be:

        CompareConstraint(CMP_EQ, [0,1,4], f1=diff)

      This constraint thus represents the constraint X0-X1 == X4.

    Example 2:
      This class can also be used to express constraints of arity 1 by using the transformation \
      function ``constant``.

        CompareConstraint(CMP_LT, [2], f2=constant(3))
        CompareConstraint(CMP_LT, [2], f1=constant(3))

      The first constraint expresses that X2 < 3, while the second expresses that X2 > 3.

    See :class:`FuncDesc` for more information on available transformation functions.

    """

    def __init__(self, compare, args, f1=None, f2=None):
        self.compare = compare
        self.args = args
        self.f1 = f1
        self.f2 = f2

    def _transform(self, args):
        if self.f1 is None:
            r1 = args[0]
            args = args[1:]
        else:
            r1 = self.f1(*args[:self.f1.arity])
            args = args[self.f1.arity:]

        if self.f2 is None:
            r2 = args[0]
        else:
            r2 = self.f2(*args[:self.f2.arity])
        return r1, r2

    def _split_args(self):
        if self.f1 is None:
            args1 = [self.args[0]]
            args2 = self.args[1:]
        else:
            args1 = self.args[:self.f1.arity]
            args2 = self.args[self.f1.arity:]
        return list(args1), list(args2)

    def check(self, example, default=True):
        """Verify whether the constraint holds on the example.

        :param example: the example to test on
        :type example: :class:`Example`
        :param default: default answer when the constraint is not applicable because relevant \
        variables are not assigned
        :return: True if the constraint is applicable and holds, False if the constraint is \
        applicable and fails, `default` if the constraint is not applicable
        """
        # Get the values of the argument variables.
        args = [example[v] for v in self.args]
        if None in args:
            # Constraint not applicable
            return default
        else:
            return self.compare.function(*self._transform(args))

    def _args_to_str(self):
        args = self.args
        if self.f1 is not None:
            arg1 = self.f1.to_str(args[:self.f1.arity])
            args = args[self.f1.arity:]
        else:
            arg1 = 'X%s' % args[0]
            args = args[1:]

        if self.f2 is not None:
            arg2 = self.f2.to_str(args)
        else:
            arg2 = 'X%s' % args[-1]
        return arg1, arg2

    def __repr__(self):
        predname = self.compare.name
        arg1, arg2 = self._args_to_str()
        return '%s(%s, %s)' % (predname, arg1, arg2)

    def to_fzn(self, cn, reif=None):
        """Transform to a string of FlatZinc code.

        :param cn: unique identifier for constraint in the model
        :type cn: any
        :param reif: name of reification variable (None if not used)
        :type reif: str
        :return: two snippets of FlatZinc: variable definitions and constraints
        :rtype: str, str
        """
        args1, args2 = self._split_args()
        cn1 = '%s_1' % cn
        cn2 = '%s_2' % cn
        fzn_vars, fzn_cons = [], []
        if self.f1 is not None:
            v1, fzn_vars1, fzn_cons1 = self.f1.to_fzn(args1, cn=cn1)
            fzn_vars += [fzn_vars1]
            fzn_cons += [fzn_cons1]
        else:
            v1 = 'X%s' % self.args[0]

        if self.f2 is not None:
            v2, fzn_vars2, fzn_cons2 = self.f2.to_fzn(args2, cn=cn2)
            fzn_vars += [fzn_vars2]
            fzn_cons += [fzn_cons2]
        else:
            v2 = 'X%s' % self.args[-1]

        cons_name = self.compare.name
        if reif:
            cons_name += '_reif'
            reif_var = ',%s' % reif
        else:
            reif_var = ''

        fzn_cons += ['constraint int_%s(%s,%s%s);' % (cons_name, v1, v2, reif_var)]
        return fzn_vars, fzn_cons

    def inverse(self):
        """Create the inverse constraint.

        :return: a constraint representing the opposite of this constraint
        :rtype: :class:`Constraint`
        """
        if self.compare == CMP_NE:
            return CompareConstraint(CMP_EQ, self.args, f1=self.f1, f2=self.f2)
        elif self.compare == CMP_EQ:
            return CompareConstraint(CMP_NE, self.args, f1=self.f1, f2=self.f2)
        elif self.compare == CMP_LT:
            a1, a2 = self._split_args()
            return CompareConstraint(CMP_LE, a2 + a1, f1=self.f2, f2=self.f1)
        elif self.compare == CMP_LE:
            a1, a2 = self._split_args()
            return CompareConstraint(CMP_LT, a2 + a1, f1=self.f2, f2=self.f1)
        else:
            raise TypeError("Unsupported comparison operator: '%s'" % self.compare.name)

    @property
    def variables(self):
        """Returns all the variables involved in the constraint.

        :return: set of variables in the constraint
        :rtype: set[int]
        """
        return set(self.args)

    def is_trivial(self):
        """Check whether the constraint is trivially true or false.
        This holds when both arguments are the same.

        :return: True if both arguments are identical (uses string-based comparison)
        :rtype: bool
        """
        arg1, arg2 = self._args_to_str()
        return arg1 == arg2

    def is_canonical(self):
        """Check whether the constraint is in canonical form.
         A constraint is in canonical form iff its arguments are in canonical form and \
         it is either non-symmetric or its arguments are in lexicographical form.
         (Lexicographical order is defined as order on the string representations.)

        For example:

            ne(X0,X1)    => True
            ne(X1,X0)    => False (symmetric and arguments not in lexicographical order)
            lt(X1,X0)    => True (not symmetric)
            eq(X1+X0,X2) => False (first argument not in canonical form)
            eq(X1-X0,X2) => True (first argument is not symmetric)
            eq(X2,X1-X0) => False (symmetric and arguments not in lexicographical order)

        :return: True if the constraint is in canonical form; False otherwise.
        :rtype: bool
        """
        args1, args2 = self._split_args()
        # Check arguments
        if self.f1 is not None and not self.f1.is_canonical(args1):
            return False
        if self.f2 is not None and not self.f2.is_canonical(args2):
            return False

        arg1, arg2 = self._args_to_str()
        if FuncDesc.SYMMETRIC in self.compare.properties and arg1 > arg2:
            # Comparison is symmetric and arguments are not in lexicographical order.
            return False
        else:
            return True

    def _same_args(self, other):
        """

        :param other:
        :type other: CompareConstraint
        :return:
        """
        a1, a2 = self._split_args()
        o1, o2 = other._split_args()
        return (a1, a2, self.f1, self.f2) == (o1, o2, other.f1, other.f2) or \
               (a1, a2, self.f1, self.f2) == (o2, o1, other.f2, other.f1)

    def _subsumes(self, other):
        """Checks whether this constraint is more specific that the given constraint.

        :param other: other constraint
        :type other: CompareConstraint
        :return: True if the current constraint is more specific than the given constraint.
        """
        # Subsumption
        #  EQ(a,b) < LE(a,b)  (eq & le)
        #  EQ(a,b) < LE(b,a)  (eq & le)
        #  LT(a,b) < NE(a,b)  (lt & ne)
        #  LT(b,a) < NE(a,b)  (lt & ne)
        #  LT(a,b) < LE(a,b)  (lt & le)
        if self == other:  # (every constraint subsumes itself)
            return True
        elif not self._same_args(other):  # (only constraints on same scope can subsume)
            return False
        elif self.compare == CMP_EQ and other.compare == CMP_LE:  # (eq & le)
            return True
        elif self.compare == CMP_LT and other.compare == CMP_NE:  # (lt & ne)
            return True
        elif self.compare == CMP_LT and other.compare == CMP_LE and self.args == other.args:
            # (lt & le)
            return True
        else:
            return False

    def _excludes(self, other):
        """Checks whether both constraints can be true at the same time.

        :param other: other constraint
        :type other: CompareConstraint
        :return: False if the constraints are mutually exclusive
        """
        # Exlusive constraints:
        #  EQ(a,b) NE(a,b)  (inverse)
        #  NE(a,b) EQ(a,b)  (inverse)
        #  LT(a,b) LE(b,a)  (inverse)
        #  LE(a,b) LT(b,a)  (inverse)
        #  EQ(a,b) LT(a,b)  (eq & lt)
        #  EQ(a,b) LT(b,a)  (eq & lt)
        #  LT(a,b) EQ(a,b)  (lt & eq)
        #  LT(b,a) EQ(a,b)  (lt & eq)
        #  LT(a,b) LT(b,a)  (lt & lt)
        if not self._same_args(other):
            return False
        elif self.inverse() == other:   # (inverse)
            return True
        elif self.compare == CMP_EQ and other.compare == CMP_LT:  # (eq & lt)
            return True
        elif self.compare == CMP_LT and other.compare == CMP_EQ:  # (lt & eq)
            return True
        elif self.compare == CMP_LT and other.compare == CMP_LT and self.args != other.args:
            # (lt & lt)
            return True
        else:
            return False

    def conjoin(self, other):
        """Combine two constraints with a conjunction.

        :param other: other constraint
        :type other: Constraint
        :return: constraint representing the conjunction of self and other
        """
        if isinstance(other, CompoundConstraint):
            cs = []
            s = self
            for c in other.components:
                assert not isinstance(c, CompoundConstraint)
                sc = s.conjoin(c)
                if isinstance(sc, CompoundConstraint):
                    cs.append(c)
                elif sc is None:
                    return None
                else:
                    s = sc
            cs.append(s)
            return CompoundConstraint(*cs)
        else:
            assert isinstance(other, CompareConstraint)
            return self._conjoin(other)

    def _conjoin(self, other):
        """Conjoin with another CompareConstraint.

        :type other: CompareConstraint
        """
        if self._excludes(other):
            # Constraints are mutually exclusive.
            return None
        elif self._subsumes(other):
            # Current constraint is more general than other.
            return self
        elif other._subsumes(self):
            # Other constraint is more general than current.
            return other
        elif self._same_args(other) and self.compare == CMP_LE and other.compare == CMP_LE:
            # LE(a,b) LE(b,a) => EQ(a,b)
            # We can simplify this combination.
            a1, a2 = self._split_args()
            c = CompareConstraint(CMP_EQ, a1+a2, f1=self.f1, f2=self.f2)
            # Make sure we have the canonical version.
            if c.is_canonical():
                return c
            else:
                return CompareConstraint(CMP_EQ, a2+a1, f1=self.f2, f2=self.f1)
        else:
            return CompoundConstraint(self, other)

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return str(self) == str(other)


class CompoundConstraint(Constraint):
    """Conjunction of other constraints.

    Components should not be compound constraints.

    :param components:
    :type components: list[Constraint]
    """

    def __init__(self, *components):
        self.components = sorted(components, key=str)

    def check(self, example, default=None):
        """Verify whether the constraint holds on the example.

        :param example: the example to test on
        :type example: :class:`Example`
        :param default: default answer when the constraint is not applicable because relevant \
        variables are not assigned
        :return: True if the constraint is applicable and holds, False if the constraint is \
        applicable and fails, `default` if the constraint is not applicable
        """
        for c in self.components:
            if not c.check(example, default):
                return False
        return True

    def to_fzn(self, cn, reif=None):
        """Transform to a string of FlatZinc code.

        :param cn: unique identifier for constraint in the model
        :type cn: any
        :param reif: name of reification variable (None if not used)
        :type reif: str
        :return: two snippets of FlatZinc: variable definitions and constraints
        :rtype: str, str
        """
        fzn_vars = []
        fzn_cons = []
        reif_vars = []
        for i, c in enumerate(self.components):
            c_cn = '%s_%s' % (cn, i)
            if reif is not None:
                c_reif = '%s_%s' % (reif, i)
                reif_vars.append(c_reif)
            else:
                c_reif = None
            c_vars, c_cons = c.to_fzn(c_cn, reif=c_reif)
            fzn_vars.append(c_vars)
            fzn_cons.append(c_cons)
        if reif is not None:
            fzn_cons.append('constraint array_bool_and([%s], %s);' % (','.join(reif_vars), reif))
        return '\n'.join(fzn_vars), '\n'.join(fzn_cons)

    @property
    def variables(self):
        """Returns all the variables involved in the constraint.

        :return: set of variables in the constraint
        :rtype: set[int]
        """
        s = set()
        for c in self.components:
            s |= c.variables
        return s

    def inverse(self):
        """Create the inverse constraint.

        :return: a constraint representing the opposite of this constraint
        :rtype: :class:`Constraint`
        """
        raise NotImplementedError('Operation not supported')

    def __repr__(self):
        return ' /\\ '.join(map(str, self.components))

    def conjoin(self, other):
        """Combine two constraints with a conjunction.

        :param other: other constraint
        :type other: Constraint
        :return: constraint representing the conjunction of self and other
        """
        if not isinstance(other, CompoundConstraint):
            return other.conjoin(self)
        else:
            s = other
            for c in self.components:
                assert not isinstance(c, CompoundConstraint)
                s = c.conjoin(s)
                if s is None:
                    return None
            return s

    def is_trivial(self):
        """Check whether the constraint is trivially true or false.
        This holds when both arguments are the same.

        :return: True if both arguments are identical (uses string-based comparison)
        :rtype: bool
        """
        return False

    def is_canonical(self):
        """Check whether the constraint is in canonical form.
         A constraint is in canonical form iff its arguments are in canonical form and \
         it is either non-symmetric or its arguments are in lexicographical form.
         (Lexicographical order is defined as order on the string representations.)

        For example:
            (note that 'eq' and '+' are symmetric and 'lt' and '-' are not)

            eq(X0,X1)    => True
            eq(X1,X0)    => False (symmetric and arguments not in lexicographical order)
            lt(X1,X0)    => True (not symmetric)
            eq(X1+X0,X2) => False (first argument not in canonical form)
            eq(X1-X0,X2) => True (first argument is not symmetric)
            eq(X2,X1-X0) => False (symmetric and arguments not in lexicographical order)

        :return: True if the constraint is in canonical form; False otherwise.
        :rtype: bool
        """
        return True

    def __iter__(self):
        return iter(self.components)


class FuncDesc(object):
    """Represents a transformation function that can be applied on the arguments of a \
    CompareConstraint.

    :param name: textual representation of the function (*)
    :type name: str
    :param function: Python function that computes the result of the transformation.
    :type function: Python function
    :param fzn_out: the name of the FlatZinc variable that contains the result of the function (*)
    :type fzn_out: str
    :param fzn_vars: variable definitions for auxiliary variables required to express the function \
    in FlatZinc (*)
    :type fzn_vars: str
    :param fzn_cons: constraint required to express the function in FlatZinc (*)
    :type fzn_cons: str
    :param properties: list of properties of this function (e.g. symmetry)

    (*) These strings can contain parameters that are replaced with the arguments, or with a \
    unique id.
    The available parameters are:

        * ``arg1``, ... ``argN``: the number of the input variable, these should always be \
        prefixed with ``X``
        * ``cn``: a unique identifier that should be used in the name of ALL auxiliary variables

    Example:

        The function that sums two variables can be described as:

            name = "X{arg1}+X{arg2}"
            function = lambda a, b: a + b
            fzn_out = "OUT_{cn}"
            fzn_vars = "var int: OUT_{cn};"
            fzn_cons = "constraint int_plus(X{arg1}, X{arg2}, OUT_{cn};"

        Note that the parameter {cn} can not be used in the ``name`` argument.

    The following transformation functions are predefined:

       * ``noop``: don't do any transformations (equivalent to setting the function to None)
       * ``constant(C)``: (arity 0) always returns constant C
       * ``plus_constant(C)``: (arity 1) adds constant C to the variable (C can be negative)
       * ``plus``: (arity 2) sums up two variables
       * ``diff``: (arity 2) takes the difference of two variables
       * ``absdiff``: (arity 2) takes the absolute difference of two variables
    """

    SYMMETRIC = 1

    def __init__(self, name, function, fzn_out, fzn_vars, fzn_cons, properties=None):
        self.name = name
        self.function = function
        self.fzn_out = fzn_out
        self.fzn_vars = fzn_vars
        self.fzn_cons = fzn_cons
        if properties is None:
            self.properties = frozenset()
        else:
            self.properties = frozenset(properties)

    @property
    def arity(self):
        """The number of input arguments expected by the object's ``function``.

        :return: arity of the constraint
        :rtype: int
        """
        return len(inspect.getargspec(self.function).args)

    def __call__(self, *args):
        return self.function(*args)

    def to_str(self, args):
        """Create a textual representation of the transformation function.

        :param args: the variables on which the function is applied
        :type args: list[int]
        :return: ``name`` attribute with parameters replaced by the arguments
        :rtype: str
        """
        d = {}
        for i, a in enumerate(args):
            d['arg%s' % (i+1)] = a
        return self.name.format(**d)

    def to_fzn(self, args, cn=None):
        """Create a FlatZinc representation of the transformation function.

        :param args: the variables on which the function is applied
        :type args: list[int]
        :param cn: an identifier that is used to make auxiliary variable names unique
        :type cn: str
        :return: name of the output variable, variable definitions, constraint definitions
        :rtype: (str,str,str)
        """
        d = {}
        for i, a in enumerate(args):
            d['arg%s' % (i+1)] = a
        d['cn'] = cn
        return self.fzn_out.format(**d), self.fzn_vars.format(**d), self.fzn_cons.format(**d)

    def is_canonical(self, args):
        """Checks whether the given list of arguments is in canonical form.
        It is in canonical form iff the function is not symmetric or if the arguments are in \
        lexicographical order.

        :param args: arguments to the function
        :return:
        """
        if FuncDesc.SYMMETRIC in self.properties and args[0] > args[1]:
            return False
        else:
            return True


noop = None  # FuncDesc('X{arg1}', lambda a: a, "X{arg1}", "", "")


def constant(c):
    """Transformation function that returns a constant.

    :param c: the constant to return
    :return: description of the function that returns the given constant
    :rtype: FuncDesc
    """
    return FuncDesc(str(c), lambda: c, str(c), "", "")


def plus_constant(c):
    """Transformation function that adds a constant to a given variable.

    :param c: the constant to add
    :return: description of the function that adds the given constant to a given variable
    :rtype: FuncDesc
    """
    if c == 0:
        return noop
    else:
        if c > 0:
            name = 'X{arg1}+%s' % c
        else:
            name = 'X{arg1}-%s' % -c

        return FuncDesc(name,
                        lambda a: a+c,
                        "OUT_{cn}",
                        "var int: OUT_{cn};",
                        "constraint int_plus(X{arg1},%s,OUT_{cn});" % c)

plus = FuncDesc('X{arg1}+X{arg2}',
                lambda a, b: a+b,
                "OUT_{cn}",
                "var int: OUT_{cn};",
                "constraint int_lin_eq([1,1,-1], [X{arg1},X{arg2},OUT_{cn}],0);",
                properties=[FuncDesc.SYMMETRIC])

diff = FuncDesc('X{arg1}-X{arg2}',
                lambda a, b: a-b,
                "OUT_{cn}",
                "var int: OUT_{cn};",
                "constraint int_lin_eq([1,-1,-1], [X{arg1},X{arg2},OUT_{cn}],0);")


absdiff = FuncDesc('abs(X{arg1}-X{arg2})',
                   lambda a, b: abs(a-b),
                   "OUT_{cn}",
                   "var int: OUT_{cn}; var int: AUX_{cn};",
                   "constraint int_lin_eq([1,-1,-1], [X{arg1},X{arg2},AUX_{cn}],0); "
                   "constraint int_abs(AUX_{cn}, OUT_{cn});",
                   properties=[FuncDesc.SYMMETRIC])

FUNC_ALL = [noop, plus, diff, absdiff]

CompareDesc = namedtuple('Comp', ('name', 'function', 'properties'))
CMP_EQ = CompareDesc('eq', lambda a, b: a == b, frozenset([FuncDesc.SYMMETRIC]))
CMP_NE = CompareDesc('ne', lambda a, b: a != b, frozenset([FuncDesc.SYMMETRIC]))
CMP_LT = CompareDesc('lt', lambda a, b: a < b, frozenset())
CMP_LE = CompareDesc('le', lambda a, b: a <= b, frozenset())
CMP_ALL = [CMP_EQ, CMP_NE, CMP_LT, CMP_LE]


def take(lst, n):
    """Take n elements from the given list (with replacement).

    :param lst: list to take elements from
    :param n: number of elements to take
    :return: generator of all possible solutions
    """
    if n == 0:
        yield ()
    else:
        for x in lst:
            for xs in take(lst, n-1):
                yield (x,) + xs


def generate_combinations(variables, compares, functions=None):
    """Generate all canonical, non-trivial constraints that are a combination of the given \
    variable, comparison operators and transformation functions.

    :param variables: variables to use in the constraints
    :param compares: list of comparison operators
    :type compares: list[CompDesc]
    :param functions: list of transformation function to use (default: [noop])
    :type functions: list[FuncDesc]
    :return: generator of requested constraints
    :rtype: generator[CompareConstraint]
    """
    if functions is None:
        functions = [noop]

    for c in compares:
        for f1 in functions:
            for f2 in functions:
                if f1 is None:
                    f1a = 1
                else:
                    f1a = f1.arity
                if f2 is None:
                    f2a = 1
                else:
                    f2a = f2.arity

                for args in take(variables, f1a + f2a):
                    cc = CompareConstraint(c, args, f1=f1, f2=f2)
                    if cc.is_canonical() and not cc.is_trivial():
                        yield cc


def generate_diagonals(variables, compares):
    """Generate constraints on the diagonals numbers (e.g. for n-queens)

    :param variables: variables to use in the constraints
    :param compares: comparison functions
    :type compares: list[CompDesc]
    :return: generator of requested constraints
    :rtype: generator[CompareConstraint]
    """
    for c in compares:
        for a, b in take(variables, 2):
            c1 = CompareConstraint(c, [a, b], f1=plus_constant(a), f2=plus_constant(b))
            if c1.is_canonical() and not c1.is_trivial():
                yield c1
            c1 = CompareConstraint(c, [a, b], f1=plus_constant(-a), f2=plus_constant(-b))
            if c1.is_canonical() and not c1.is_trivial():
                yield c1


def split_set(lst):
    """Split the set in two equal parts.

    :param lst: set to split
    :type lst: Sequence[T]
    :return: two sets
    :rtype: (set[T],set[T])
    """
    c = (len(lst)+1) / 2
    s1 = set()
    s2 = set()
    for v in lst:
        if c > 0:
            s1.add(v)
            c -= 1
        else:
            s2.add(v)
    return s1, s2


def pick(lst, n):
    """Pick n elements from the input sequence (with replacement).

    :param lst: input sequence
    :type lst: Sequence[T]
    :param n: number of elements in output list
    :type n: int
    :return: all possible solutions
    :rtype: generator[tuple[T]]
    """
    if n == 0:
        yield ()
    else:
        for x in lst:
            for xs in pick(lst, n-1):
                yield (x,) + xs


class ProcessInterrupted(Exception):
    """Custom Exception to indicate that the subprocess was interrupted by a Timeout."""
    pass


class CallProcess(object):
    """Helper class for calling a subprocess.

    :param cmd: command to call
    :type cmd: list[str]
    :param cleanup: files to delete at end of process
    :type cleanup: list[str]
    :param timeout: amount of processing time (in seconds) that the subprocess is allowed to use
    :type timeout: int
    """

    def __init__(self, cmd, cleanup=(), timeout=0):
        self.cmd = cmd
        self.cleanup = cleanup
        self.timeout = timeout
        self.retcode = None
        self.process = None

    def __enter__(self):
        if self.timeout:
            def _setlimits():
                resource.setrlimit(resource.RLIMIT_CPU, (self.timeout, self.timeout))
            self.process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, preexec_fn=_setlimits)
        else:
            self.process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE)
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, *args):
        if self.process is not None:
            try:
                self.process.kill()
            except OSError:
                pass
            # self.fnull.close()
            if self.process.stdout:
                self.process.stdout.close()
            if self.process.stderr:
                self.process.stderr.close()
            self.process = None
        for f in self.cleanup:
            os.remove(f)
        if self.retcode == -24:
            raise ProcessInterrupted()

    def __iter__(self):
        for line in self.process.stdout:
            yield line
        self.retcode = self.process.wait()


# noinspection PyUnusedLocal
def call_fzn(fzn, size, findall=False, solver=None, max_examples=None, timeout=None, **extra):
    """Helper method for calling a FlatZinc-based constraint solver.

    :param fzn: FlatZinc code
    :type fzn: str
    :param size: number of variables
    :type size: int
    :param findall: find all solutions or only one
    :type findall: bool
    :param solver: solver to use (see SOLVERS)
    :type solver: str
    :param max_examples: maximum number of examples to retrieve (assumes findall)
    :type max_examples: int
    :param timeout: maximum processing time allowed
    :type timeout: int
    :param extra: additional options (not used)
    :return: array of variable assignment
    :rtype: generator[:class:`Example`]
    """
    f = tempfile.NamedTemporaryFile('w', suffix='.fzn', delete=False)
    f.write(fzn)
    f.close()

    cmd = SOLVERS[solver][:]

    if findall:
        if max_examples:
            if solver == 'gecode':
                cmd += ['-n', '%s' % max_examples]
            else:
                cmd += ['-n=%s' % max_examples]
        else:
            cmd += ['-a']
    cmd += [f.name]

    n = 0
    with CallProcess(cmd, cleanup=[f.name], timeout=timeout) as output:
        example = [None]*size
        for line in output:
            if not line.strip().startswith('===') and '=' in line:
                var, val = line.strip().split('=', 1)
                var = int(var.strip()[1:])
                val = int(val.strip()[:-1])
                example[var] = val
            elif line.strip().startswith('---'):
                n += 1
                yield Example(example)
                # if n % 10000 == 0:
                #     print ('examples generated: %s' % n)
                example = [None]*size


def add_arguments(argparser):
    """Add default arguments to the given argument parser.

    :param argparser: argument parser to extend
    :type argparser: :class:`argparse.ArgumentParser`
    """
    argparser.add_argument('--solver', choices=SOLVERS.keys(), default=DEFAULT_SOLVER,
                           help="underlying CP solver")
    argparser.add_argument('--max-examples', type=int, default=0,
                           help="give up search after this many examples have been tried")
    argparser.add_argument('--strategy', choices=['implied', 'postprocess', 'maxfail'],
                           default='postprocess', help="example generation strategy")
    argparser.add_argument('--timeout', type=int, default=0,
                           help="timeout on find_example process")
    argparser.add_argument('-o', '--output', type=str, default=None,
                           help="write final FlatZinc model to given file")
    argparser.add_argument('-v', '--verbose', action='count', help="Verbosity level.")
