# README #

This repository contains an implementation of the QuAcq algorithm.

### Requirements ###

* Recent version of Python or PyPy (recommended)
* Gecode (fzn-gecode available on PATH)

### Installation ###

No installation required.

### Usage ###

Run one of the example scripts from the ``examples/`` folder.
Use ``--help`` for options.

### License ###

Copyright 2015 Anton Dries (KU Leuven)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.