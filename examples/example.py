#! /usr/bin/env python

from __future__ import print_function

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from ducq import *

def oracle_function(example):
    r1 = (example[0] is None or example[4] is None or example[0] == example[4])
    r2 = (example[2] is None or example[3] is None or example[2] != example[3])
    return r1 and r2


def create_example(interactive=False, **kwdargs):
    variables = set(range(0, 5))
    bias = list(generate_combinations(variables, CMP_ALL, noop))
    domains = [range(1, 6) for v in variables]
    if interactive:
        oracle = CLIOracle()
    else:
        oracle = FunctionBasedOracle(oracle_function)
    return LearningTask(domains, oracle, bias, **kwdargs)

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser()
    add_arguments(argparser)
    argparser.add_argument('--interactive', action='store_true')
    args = argparser.parse_args()

    create_example(**vars(args)).run()
