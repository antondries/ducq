#! /usr/bin/env python

"""
Specifies an unspecified learning problem.
The problem is completely determined by the users responses to the queries.
"""

from __future__ import print_function

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from ducq import *

def create_generic(n, mn, mx, **kwdargs):
    """Create an unspecified learning problem.

    :param n: number of variables
    :param mn: minimum value in domains
    :param mx: maximum value in domains
    :param kwdargs: additional parameters
    :return: generic learning task
    """
    variables = range(0, n)
    domains = [list(range(mn, mx+1)) for _ in variables]
    bias = list(generate_combinations(variables, CMP_ALL, FUNC_ALL))
    bias += list(generate_diagonals(variables, CMP_ALL))
    return LearningTask(domains, CLIOracle(), bias, **kwdargs)


if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('n', type=int, default=4, nargs='?',
                           help="number of variables")
    argparser.add_argument('mn', type=int, default=0, nargs='?',
                           help="minimum value in domain")
    argparser.add_argument('mx', type=int, default=4, nargs='?',
                           help="maximum value in domain")
    add_arguments(argparser)
    args = argparser.parse_args()

    create_generic(**vars(args)).run()
