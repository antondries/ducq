#! /usr/bin/env python

"""
N-queens
"""


from __future__ import print_function

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from ducq import *


def create_nqueens(n=2, interactive=False, **kwdargs):

    domains = [list(range(0, n)) for i in range(0, n)]

    def check_queens(example):
        """Check n-queens solution.

        :param example: example to check
        :return: True if the example could be extended to a valid n-queens solution.
        """
        values = [x for x in example]
        for i, v1 in enumerate(values):
            for j, v2 in enumerate(values[i+1:]):
                j += i+1
                if v1 is not None and v2 is not None:
                    if v1 == v2:
                        return False
                    elif abs(i-j) == abs(v1-v2):
                        return False
        return True

    bias = list(generate_diagonals(range(0, n), CMP_ALL))
    bias += list(generate_combinations(range(0, n), CMP_ALL))

    if interactive:
        oracle = CLIOracle(queen_formatter)
    else:
        oracle = FunctionBasedOracle(check_queens)
    return LearningTask(domains, oracle, bias, **kwdargs)

def queen_formatter(example):
    n = len(example)

    s = '+' + '-' * n + '+\n'
    for i, q in enumerate(example):
        if q is None:
            s += '|%s|\n' % (' ' * n)
        else:
            row = [' '] * n
            row[q] = 'Q'
            s += '|%s|\n' % ''.join(row)
    s += '+' + '-' * n + '+\n'
    return s


if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('n', type=int, default=4, nargs='?',
                           help="number of queens")
    argparser.add_argument('--interactive', action='store_true',
                           help="ask queries interactively")
    add_arguments(argparser)
    args = argparser.parse_args()

    create_nqueens(**vars(args)).run()


# v1-i != v2-j
# v1+i != v2+j
