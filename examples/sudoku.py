#! /usr/bin/env python

"""
Specification of the Sudoku puzzle.
"""

from __future__ import print_function

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from ducq import *

import sys


def create_sudoku(n, **kwdargs):
    """
    Create a Sudoku problem.
    :param n: dimension of one sub-block (e.g. standard Sudoku is 3)
    :param kwdargs: additional arguments
    :return: sudoku problem
    """
    size = n*n

    domains = [range(1, size+1) for _ in range(0, size*size)]

    real_constraints = []
    for i in range(0, size):
        row_vars = [rowcol2index(i, j, size) for j in range(0, size)]
        col_vars = [rowcol2index(j, i, size) for j in range(0, size)]
        blk_vars = [blockindex2index(i, j, n) for j in range(0, size)]

        real_constraints += generate_combinations(row_vars, [CMP_NE])
        real_constraints += generate_combinations(col_vars, [CMP_NE])
        real_constraints += generate_combinations(blk_vars, [CMP_NE])

    oracle = ModelBasedOracle(ConstraintSet(real_constraints))

    bias = generate_combinations(range(0, size*size), CMP_ALL, [noop])
    return LearningTask(domains, oracle, bias, **kwdargs)


def rowcol2index(row, col, size):
    """Translate (row, col) position to variable index.
    :param row: row number
    :type row: int
    :param col: column number
    :type col: int
    :param size: size of row
    :type size: int
    :return: variable index for the cell at the given row, col position
    :rtype: int
    """
    return row * size + col


def blockindex2index(block, cell, n):
    """Translate (block, cell) position to variable index
    :param block: block number
    :type block: int
    :param cell: cell number in block
    :type cell: int
    :param n: size of block
    :type n: int
    :return: index of variable
    :rtype: int
    """
    b_row = block // n
    b_col = block % n
    return rowcol2index(b_row*n + cell // n, b_col*n + cell % n, n*n)


if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('n', type=int, default=2, nargs='?',
                           help="dimension of subblock of Sudoku (default: 2 (= 4x4 Sudoku))")
    add_arguments(argparser)
    args = argparser.parse_args()

    if len(sys.argv) > 1:
        create_sudoku(**vars(args)).run()
    else:
        create_sudoku(**vars(args)).run()
