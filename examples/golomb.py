#! /usr/bin/env python

"""
A Golomb ruler may be defined as a set of m integers 0 = a_1 < a_2 <
... < a_m such that the m(m-1)/2 differences a_j - a_i, 1 <= i < j
<= m are distinct. Such a ruler is said to contain m marks and is of
length a_m. The objective is to find optimal (minimum length) or
near optimal rulers.
"""


from __future__ import print_function

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from ducq import *


def create_golomb(m=2, maxvalue=10, canonical=False, **kwdargs):
    """Create a learning problem for the Golomb ruler.

    :param m: size of the ruler
    :param maxvalue: maximum value of the ruler
    :param kwdargs: additional options
    :return: learning task representing this problem
    """

    domains = [list(range(0, maxvalue+1)) for i in range(0, m)]

    def check_ruler(example):
        """Check Golomb ruler.

        :param example: example to check
        :return: True if the example could be extended to a valid ruler.
        """
        values = [x for x in example if x is not None]
        distances = set()
        for i, v1 in enumerate(values):
            for v2 in values[i+1:]:
                if v2 <= v1:
                    return False
                d = v2 - v1
                if d in distances:
                    return False
                else:
                    distances.add(d)
        if canonical and example[0] is not None and example[1] is not None and \
                example[-1] is not None and example[-2] is not None \
                and example[1] - example[0] > example[-1] - example[-2]:
            return False
        return True

    bias = list(generate_combinations(range(0, m), CMP_ALL, [noop]))
    bias += list(generate_combinations(range(0, m), CMP_ALL, [diff]))
    oracle = FunctionBasedOracle(check_ruler)
    return LearningTask(domains, oracle, bias, **kwdargs)


if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('m', type=int, default=5, nargs='?',
                           help="size of ruler")
    argparser.add_argument('maxvalue', type=int, default=11, nargs='?',
                           help="maximum value on ruler")
    argparser.add_argument('--canonical', action='store_true',
                           help="learn canonical rulers")

    add_arguments(argparser)
    args = argparser.parse_args()

    create_golomb(**vars(args)).run()
